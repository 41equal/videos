
## Streaming
### Jitsi:
- can see less in 'spotlighted' mode (singular view who ever is speaking) vs.  fullscreen / gallery


## Youtube 
- to add direct url links, need to have 100 subscribers
- partner program (monetization) requires 1000 subs
- recently they changed their policies to give them permission to have ads (without sharing revenue...) 


## Camera / Hardware investigation
- [high quality raspberry pi camera](https://www.raspberrypi.org/products/raspberry-pi-high-quality-camera/) which has a 7.9 mm sony sensor (still small) but it has interchangeable lenses

### Dongles
- 4k dongles exist ~$100
- $20 1080p
- most cameras wont go straight to linux (cause drivers etc)

### Panasonic comparisons
- G85 can record (internally) and stream simultaneously
- g7 comparable but cant record and stream at same time
- g5 cannot stream
- g6 


## Camera Angles:
### Closeups: 
- face out of sight and fingerboard aimed away from camera
- sony camcorder has a flat focus in comparison with the g85 which can zoom in and unfocus the background
	- blurrier the background 

# General Video Production Notes
- wolftune: with separated ear-pieces, I could wear only the left ear to hear things (input from others watching me, backing tracks whatever) and with my head turned left toward the guitar, it won't be visible on the video
- for close ups on vidchat, can join with a mobile device and zoom in (on fretboard, etc) simultaneously

- lenses are pretty simple: to get a nice really blurred background at a wide angle (seeing the whole guitar and such), the prime lens is needed (the whole point is that has a wider aperture that can do that narrow focus). - But aside from that effect, there's not enough difference with the lenses to care. The kit lens is great.
- I really like the effect of the prime lens for some of what I'll record probably, but with just the kit lens, here's the ways to get a great shot:
  - have all the background stuff be fine being in focus because it just looks nice and interesting without being distracting too much
  - get some blurred background anyway by using a large space where the subject is still quite far from the background even when also far enough from the camera to have it zoomed in (zoom narrows the focus depth, similar to how aperture does)
  - use a backdrop so that there's no concern about what's in the background or what's in focus or not
Cheaper, easier, and less to fuss with by getting backdrop(s) instead of messing with various lenses

3/4/21 @wolftune
wow, I got my Azden SMX-30 mic today and did a quick test for audio quality. It's superb, quite great. I mean, no noise really, clean even sound, not boomy, crisp enough. This was a great pragmatic satisficing decision. The unique thing is this one device has 3 mics: a shotgun mic and a stereo matched pair. It has a simple toggle to switch between those two options. The quality is good enough to just use the mic for many things, might as well use it any time I'm recording on the camera itself. So, for some things I will also record with other mics direct to the computer, but it makes no sense to have the audio that the camera records be something other than this mic, might as well have that option for the mix.

I got a used one for $180. I just now noticed that for a $300 new price the SMX-30V is the same thing with a mix knob to combine the stereo or the shotgun, and that's awesome. If money were irrelevant and I had a newer G9 of higher-end GH5 or other camera with a headphone jack (the G85 has mic input but no headphone jack!)
anyway, for outdoor and portable recording of videos, this setup is overall great. Some day eventually an upgrade to a newer camera with headphone jack would make the mix-able version of the mic more worthwhile because could then actually tell what it was mixing. Would be worth it regardless if found at a discount

although headphone jack would really be preferable, the full-blown better-audio is a dedicated audio device which could both send output to the camera and to headphones and probably record directly itself, so like either the normal laptop and interface etc or a dedicated portable audio recorder that has output to the camera and separate headphone jack
once we get into that or even a separate preamp in order to have a good headphone amp, we're outside the realm of nice, practical, quick good-enough setup
direct monitoring of what's going into the G85 is possible via an HDMI adapter that includes audio, but it still needs a headphone amp, so it's not worth the hassle compared to just having a dedicated preamp or audio recorder
on another video production note: if we do get any extra cameras (or even consider keeping the G5 around, but meh, not sure there), the one big benefit is recording a single performance from multiple angles. Nothing to worry about right away, and we have two G85s to work with once we get together in person again (plus borrowing gear from other people and maybe the people themselves too)
I just tested the (proprietary but gratis) remote control app for the G85, and it's actually superb
newer cameras offer bluetooth, but this one is wifi only. What it does is, the camera generates a wifi hotspot, the phone disconnects from other wifi and connects to the camera's wifi, so there's no connection to the outside internet (good for privacy and security concerns!), and it can literally just be the G85's display. Like it shows the image from the camera, it has all the settings (white balance, focus, color, etc etc) on the phone
I can literally tap on the phone screen to adjust where the focus is on the camera. It can't zoom of course because on this camera, zoom is a physical control (there are lenses available with power zoom that do work with this camera, but that's not most of them
